<?php
/**
 * Created by PhpStorm.
 * User: jinfeizhang
 * Date: 28/05/2018
 * Time: 16:26
 */

namespace Chancolib\Config;


use Psr\Container\ContainerInterface;

class ConfigLoader
{
    private $container;
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }
    public function load(){
        
        $em = $this->container->get('doctrine')->getEntityManager();
        $setting = $em->getRepository('ChancoEntity:GlobalSetting')->getSetting();
        return $setting;
    }
}