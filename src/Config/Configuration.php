<?php
/**
 * Created by PhpStorm.
 * User: jinfeizhang
 * Date: 22/04/2018
 * Time: 12:01
 */
namespace Chancolib\Config;

class Configuration
{
    public static $redis_info;
    public static $config;
    public static function configurate($config){
        if(isset($config['redis_info'])){
            self::$redis_info = $config['redis_info'];
        }

        if (!isset(static::$config)) {
            static::$config = $config;
        }
    }

    public static function has($key)
    {
        return null !== static::get($key);
    }

    public static function get($configValueKey)
    {
        $keys = explode('.', $configValueKey);

        $value = static::$config;

        foreach ($keys as $key) {
            if (!array_key_exists($key, $value)) {
                return;
            }

            $value = $value[$key];
        }

        return $value;
    }


}