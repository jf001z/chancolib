<?php
/**
 * Created by PhpStorm.
 * User: jinfeizhang
 * Date: 10/08/2018
 * Time: 21:07
 */
namespace Chancolib\ImageService;

use Chancolib\AwsService\AwsS3Client;
use Ramsey\Uuid\Uuid;

class ImageService
{
    private $client;
    private $bucket_name;
    private $thumbnail_endpoint;
    private $file_key;
    public function __construct()
    {
        $this->client = AwsS3Client::getInstance();
        $this->bucket_name  = AwsS3Client::getBucket();
        $this->thumbnail_endpoint = AwsS3Client::getThumbnailEndpoint();
        $this->file_key = '';
    }

    public function downaloadImage($url,$image_file){
        $fp = fopen ($image_file, 'w+');
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_FILE, $fp);          // output to file
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 1000);      // some large value to allow curl to run for a long time
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0');
        // curl_setopt($ch, CURLOPT_VERBOSE, true);   // Enable this line to see debug prints
        curl_exec($ch);
        curl_close($ch);
        fclose($fp);
        return $image_file;
    }

    public function uploadImageToS3($image_file){
        //echo $image_file;
        if(file_exists($image_file)){

            try {
                $file = explode('.',$image_file);
                $extesion = $file[1];
                $file_name = Date('Y').'/'.Date('M').'/'.Date('d').'/'.Uuid::uuid1().'.'.$extesion;
                $this->file_key = $file_name;
                $result = $this->client->putObject([
                    'Bucket'     =>$this->bucket_name,
                    'Key'        => $file_name,
                    'SourceFile' => $image_file,    // like /var/www/vhosts/mysite/file.csv
                    'ACL'        => 'public-read',
                ]);

                $url = $result['ObjectURL'];
                //var_dump($url);
                unlink($image_file);
                return $url;

            } catch (S3Exception $e) {
                echo $e->getMessage();
                die();
            }
        }else{

        }
    }
    public function getFileS3Key(){
        return $this->file_key;
    }
    public function getImageTumbnail(){
        $curl = curl_init();
        $path = $this->file_key;
        curl_setopt_array($curl, array(
            CURLOPT_URL => $this->thumbnail_endpoint,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_HTTPHEADER => array(
                "Cache-Control: no-cache",
                "Content-Type: application/json",
                "path: $path"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
            die();
        } else {
            return $response;
        }
    }
}