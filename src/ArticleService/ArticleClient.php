<?php
/**
 * Created by PhpStorm.
 * User: jinfeizhang
 * Date: 03/07/2018
 * Time: 22:03
 */

namespace Chancolib\ArticleService;


use AYLIEN\TextAPI;
use Chancolib\Config\Configuration;


class ArticleClient
{
    protected $client;
    protected $setting;
    public function __construct()
    {
        $this->setting = Configuration::get('text_analysis_api');
        $this->getClient();

    }

    public function getClient(){

        $this->client = new TextAPI($this->setting['api_id'],$this->setting['api_key']);
        return $this->client;
    }
    public function getContent($url){
        $params = array('url'=>$url);
        $content = $this->client->Extract($params);
        return $content->article;
    }

}