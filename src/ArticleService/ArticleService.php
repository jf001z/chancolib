<?php
/**
 * Created by PhpStorm.
 * User: jinfeizhang
 * Date: 28/06/2018
 * Time: 20:35
 */

namespace Chancolib\ArticleService;


use Chancolib\Cache\Cache;
use Chancolib\Config\Configuration;
use Chancolib\Entity\Article;
use Chancolib\Entity\Content;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ArticleService
{
    protected $container;
    protected $repository;
    protected $setting;
    protected $client;
    protected $em;
    protected $cache;


    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->setting = Configuration::get('news_api_info');
        $this->getClient();
        $this->em = $this->container->get('doctrine')->getEntityManager();
        $this->repository = $this->em->getRepository('ChancoEntity:Article');
        $this->cache = new Cache();
    }


    public function getClient(){
        $this->client = new ArticleClient();
    }


    public function getContentById($id){
        $article = $this->getAticleUrlById($id);
        $contentId = null;
        if(!is_null($article)){

            //Be sure not duplicate content to one article.

            if($article->getProcessed() != 1){
                $contentId = $this->getContentByArticle($article);
            }else{
                $content = $this->em->getRepository('ChancoEntity:Content')->getContentByArticleId($id);
                $contentId = $content->getContentId();
            }
        }

        return $contentId;
    }
    
    public function getImageUrlById($id){
        $article = $this->getAticleUrlById($id);
        return $article->getImageUrl();
    }

    public function createAllArticleCache(){
        $sourceCodes = $this->getAllSource();
        foreach ($sourceCodes as $sourceCode){
            $this->rebuildCacheBySourceCode($sourceCode);
        }
    }

    public function createArticleCacheBySourceCode($sourceCode){

        $sourceCode = $this->em->getRepository('ChancoEntity:Source')->getSourceBySourceCode($sourceCode);
        if(empty($sourceCode)){
            return null;
        }
        $this->rebuildCacheBySourceCode($sourceCode[0]);
        return 1;
    }

    public function rebuildCacheBySourceCode($sourceCode){


        $articles = $this->getLatestArticlesById($sourceCode['sourceId']);
        $articleCacheKeys = array();
        $mediaCacheKey = 'media:'.$sourceCode['sourceCode'];
        $this->clearMediaCache($mediaCacheKey);
        foreach ($articles as $article){
            $articleCacheKeys[] = $this->createArticleCache($article);
        }
        $this->createMediaCache($mediaCacheKey,$articleCacheKeys);
    }

    public function getAllArticles(){
        $articles = $this->getAllArticlesFromCache();
        if(is_null($articles)){
            $this->createAllArticleCache();
        }
        $articles = $this->getAllArticlesFromCache();
        return $articles;
    }

    public function getAllArticlesFromCache(){
        $cache = $this->cache;
        $mediaKeys = $cache->getKeys('media:');
        if(empty($mediaKeys)){
            return null;
        }
        $articles = array();
        foreach ($mediaKeys as $mediaKey){
            $articleKeys = json_decode($cache->get($mediaKey),true);
            foreach ($articleKeys as $articleKey){
                $tempArticle = $cache->get($articleKey);
                $articles[str_replace('media:','',$mediaKey)][] = json_decode($tempArticle,true);
            }
        }
        return $articles;
    }

    public function getArticleFromCache($cacheKey){

    }

    public function clearMediaCache($mediaCacheKey){
        $cache = $this->cache;
        if($cache->has($mediaCacheKey)){
            $articleKeys = json_decode($cache->get($mediaCacheKey),true);
            if(!empty($articleKeys)){
                foreach ($articleKeys as $articleKey){
                    $cache->delete($articleKey);
                }
            }
            $cache->delete($mediaCacheKey);
        }
    }

    public function createArticleCache($article){
        $cache = $this->cache;
        $cacheKey = 'article:'.$article['articleId'];
        $jsonArticle = json_encode($article);
        if($cache->has($cacheKey)){
            $cache->delete($cacheKey);
        }
        $cache->set($cacheKey,$jsonArticle);
        return $cacheKey;
    }

    public function createMediaCache($mediaCacheKey, $articleKeys){
        $jsonArticleKeys = json_encode($articleKeys);
        $this->cache->set($mediaCacheKey, $jsonArticleKeys);
    }

    public function getAllSource(){
       return $this->em->getRepository('ChancoEntity:Source')->getAllSourcesCodes();
    }

    public function getAllSourceInCache(){
        return $this->em->getRepository('ChancoEntity:Source')->getAllSourcesCodesWithCache();
    }

    public function getLatestArticlesById($souceId){
        return $this->repository->getLatestArticlesById($souceId);
    }

    public function getContentByArticle(Article $article){
        $content = $this->extractContent($article);
        $contentId = $this->saveContentToDB($article,$content);
        if(!empty($contentId)){
            $this->updateArticleDB($article);
        }
        return $contentId;
    }

    public function updateArticleDB(Article $article){
        $article->setProcessed('1');
        $this->em->merge($article);
        $this->em->flush();
    }

    public function extractContent(Article $article){

        return $this->client->getContent($article->getUrl());
    }

    public function getAticleUrlById($id){
        $repository = $this->repository;
        $article = $repository->findOneBy(array('articleId'=>$id));
        if(empty($article)){
            return null;
        }
        return $article;
    }

    public function saveContentToDB(Article $article, $rawContent){
        $content = new Content();
        $content->setArticleId($article->getArticleId());
        $content->setContent($rawContent);
        $content->setArticle($article);
        $this->em->persist($content);
        $this->em->flush();
        return $content->getContentId();
    }

    public function updateArticleS3ImgThumbnail($articleId, $s3Url,$thumbnailUrl){
        $article = $this->getAticleUrlById($articleId);
        $article->setImageS3Url($s3Url);
        $article->setThumbnailUrl($thumbnailUrl);
        $this->em->persist($article);
        $this->em->flush();
        return $article;
    }
    public function getAllUnprocessedArtiles(){
        return $this->repository->getAllUnprocessedArticles();
    }

    public function get10UnprocessedImageArticles(){
        return $this->repository->get10UnprocessedImageArticles();
    }
/*
    public function getRawContent(Article $article){

        $url = $article->getUrl();

        try{
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => $url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_VERBOSE => 0,
                CURLOPT_FOLLOWLOCATION => 1,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_CUSTOMREQUEST => "GET",
                CURLOPT_HTTPHEADER => array(
                    "Cache-Control: no-cache",
                    "Postman-Token: 3f418090-cbf6-4ad7-b865-bfd962a0b9e0"
                ),
            ));

            $response = curl_exec($curl);
            $err = curl_error($curl);

            curl_close($curl);

        }catch (\Exception $e){

        }


        if ($err) {
            return null;
        }else{
            return $response;
        }
    }
*/
}