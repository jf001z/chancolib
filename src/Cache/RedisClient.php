<?php
/**
 * Created by PhpStorm.
 * User: jinfeizhang
 * Date: 22/04/2018
 * Time: 16:31
 */

namespace Chancolib\Cache;

use Predis\Client;
use Chancolib\Config\Configuration;

class RedisClient
{
    private static $client = null;

    public static function getInstance(){
        if (!empty(Configuration::$redis_info['redis_link'])){
            $conn_string = Configuration::$redis_info['redis_link'];
        }else{
            $conn_string = 'tcp://'.Configuration::$redis_info['redis_host'].':'.Configuration::$redis_info['redis_port'];
        }
        static::$client = new Client($conn_string);
        return static::$client;
    }

}