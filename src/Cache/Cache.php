<?php
/**
 * Created by PhpStorm.
 * User: jinfeizhang
 * Date: 22/04/2018
 * Time: 16:43
 */

namespace Chancolib\Cache;

use Predis\Collection\Iterator;

class Cache
{
    private  $redis;
    public function __construct()
    {
        $this->redis = RedisClient::getInstance();
    }

    public function set($key,$value){
        try{
            return $this->redis->set($key,$value);
        }catch (Exception $ex){

            die($ex->getMessage());
        }

    }
    public function has($key){
        try{
            return $this->redis->exists($key);
        }catch (Exception $ex){
            die($ex->getMessage());
        }

    }
    public function delete($key){
        try{
            return $this->redis->del($key);
        }catch (Exception $ex){
            die($ex->getMessage());
        }

    }
    public function get($key){
        try{
            return $this->redis->get($key);
        }catch (Exception $ex){
            die($ex->getMessage());
        }

    }

    public function getKeys($inputKey){
        $keys = array();
        foreach (new Iterator\Keyspace($this->redis, "$inputKey*") as $key){
            $keys[] = $key;
        }
        return $keys;
    }

}