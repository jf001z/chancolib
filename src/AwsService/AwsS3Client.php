<?php
/**
 * Created by PhpStorm.
 * User: jinfeizhang
 * Date: 10/08/2018
 * Time: 20:33
 */
namespace Chancolib\AwsService;

use Aws\S3\S3Client;
use Chancolib\Config;

class AwsS3Client
{
    static private $client;
    static private $setting;
    static public function getInstance()
    {
        self::getSetting();
        self::$client = S3Client::factory(
            array(
                'credentials' =>array(
                    'key'=>self::$setting['key'],
                    'secret'=>self::$setting['secrete']
                ),
                'region'=>'eu-west-1',
                'version'=>'latest'
            )
        );
        return self::$client;
    }
    static private function getSetting(){
        if(Config\Configuration::has('aws_keys')){
            self::$setting = Config\Configuration::get('aws_keys');
        }
    }
    static public function getBucket(){
        self::getSetting();
        return self::$setting['bucket'];
    }
    static public function getThumbnailEndpoint(){
        self::getSetting();
        return self::$setting['thumbnail_endpoint'];
    }

}