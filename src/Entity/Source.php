<?php
/**
 * Created by PhpStorm.
 * User: jinfeizhang
 * Date: 27/05/2018
 * Time: 14:49
 */

namespace Chancolib\Entity;


use Doctrine\Common\Collections\ArrayCollection;

class Source
{
    private $sourceId;
    private $sourceName;
    private $sourceCode;
    private $updateDate;
    private $articles;
    public function __construct()
    {
        $this->articles = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getSourceId()
    {
        return $this->sourceId;
    }

    /**
     * @return mixed
     */
    public function getSourceName()
    {
        return $this->sourceName;
    }

    /**
     * @param mixed $sourceName
     */
    public function setSourceName($sourceName): void
    {
        $this->sourceName = $sourceName;
    }

    /**
     * @return mixed
     */
    public function getSourceCode()
    {
        return $this->sourceCode;
    }

    /**
     * @param mixed $sourceCode
     */
    public function setSourceCode($sourceCode): void
    {
        $this->sourceCode = $sourceCode;
    }

    /**
     * @return mixed
     */
    public function getUpdateDate()
    {
        return $this->updateDate;
    }

    /**
     * @param mixed $updateDate
     */
    public function setUpdateDate($updateDate): void
    {
        $this->updateDate = $updateDate;
    }

}