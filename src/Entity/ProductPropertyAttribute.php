<?php

namespace Chancolib\Entity;


use Doctrine\ORM\Mapping as ORM;

/**
 *
 */
class ProductPropertyAttribute
{
    /**
     * @var integer
     */
    private $productPropertyAttributeId;
    /**
     *
     */
    private $productId;
    /**
     *
     */
    private $productPropertyId;
    /**
     *
     */
    private $ProductPropertyAttributeValue;
    /**
     *
     */
    private $lastUpdated;

    private $Product;

    /**
     * @return mixed
     */
    public function getProduct()
    {
        return $this->Product;
    }

    private $ProductProperty;

    /**
     * @return mixed
     */
    public function getProductProperty()
    {
        return $this->ProductProperty;
    }



    public function getProductPropertyAttributeId(): int
    {
        return $this->productPropertyAttributeId;
    }

    public function getProductId()
    {
        return $this->productId;
    }

    /**
     * @param mixed $productId
     */
    public function setProductId($productId)
    {
        $this->productId = $productId;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getProductPropertyId()
    {
        return $this->productPropertyId;
    }

    /**
     * @param mixed $productPropertyId
     */
    public function setProductPropertyId($productPropertyId)
    {
        $this->productPropertyId = $productPropertyId;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getProductPropertyAttributeValue()
    {
        return $this->ProductPropertyAttributeValue;
    }

    /**
     * @param mixed $ProductPropertyAttributeValue
     */
    public function setProductPropertyAttributeValue($ProductPropertyAttributeValue)
    {
        $this->ProductPropertyAttributeValue = $ProductPropertyAttributeValue;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getLastUpdated()
    {
        return $this->lastUpdated;
    }

    /**
     * @param mixed $lastUpdated
     */
    public function setLastUpdated($lastUpdated)
    {
        $this->lastUpdated = $lastUpdated;
        return $this;
    }


}
?>