<?php
/**
 * Created by PhpStorm.
 * User: jinfeizhang
 * Date: 27/05/2018
 * Time: 16:11
 */

namespace Chancolib\Entity;


use Doctrine\ORM\EntityRepository;

class ContentRepository extends EntityRepository
{
    public function getContentByArticleId($articleId){
        return $this->findOneBy(array('articleId'=>$articleId));
    }
}