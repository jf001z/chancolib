<?php
/**
 * Created by PhpStorm.
 * User: jinfeizhang
 * Date: 27/05/2018
 * Time: 15:03
 */

namespace Chancolib\Entity;


class Content
{
    private $contentId;
    private $articleId;
    private $content;
    private $article;
    /**
     * @return mixed
     */
    public function getContentId()
    {
        return $this->contentId;
    }
    /**
     * @return mixed
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param mixed $content
     */
    public function setContent($content): void
    {
        $this->content = $content;
    }

    /**
     * @return mixed
     */
    public function getArticleId()
    {
        return $this->articleId;
    }

    /**
     * @param mixed $articleId
     */
    public function setArticleId($articleId): void
    {
        $this->articleId = $articleId;
    }

    /**
     * @return mixed
     */
    public function getArticle()
    {
        return $this->article;
    }

    /**
     * @param mixed $article
     */
    public function setArticle($article): void
    {
        $this->article = $article;
    }


}