<?php

namespace Chancolib\Entity;

use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 *
 */
class Product
{
    /**
     * @var integer
     */
    private $productId;
    /**
     *
     */
    private $productSku;
    /**
     *
     */
    private $parentProductId;
    /**
     *
     */
    private $lastUpdated;

    /**
     * @return mixed
     */
    private $productPropertyAttributes;


    public function __construct()
    {
        $this->productPropertyAttributes = new ArrayCollection();
    }


    /**
     * @return mixed
     */
    public function getProductPropertyAttributes()
    {
        return $this->productPropertyAttributes;
    }


    public function getProductId(): int
    {
        return $this->productId;
    }

    /**
     * @return mixed
     */
    public function getProductSku()
    {
        return $this->productSku;
    }

    /**
     * @return mixed
     */
    public function getParentProductId()
    {
        return $this->parentProductId;
    }

    /**
     * @return mixed
     */
    public function getLastUpdated()
    {
        return $this->lastUpdated;
    }

    /**
     * @param mixed $productSku
     */
    public function setProductSku($productSku): void
    {
        $this->productSku = $productSku;
    }

    /**
     * @param mixed $parentProductId
     */
    public function setParentProductId($parentProductId): void
    {
        $this->parentProductId = $parentProductId;
    }

    /**
     * @param mixed $lastUpdated
     */
    public function setLastUpdated($lastUpdated): void
    {
        $this->lastUpdated = $lastUpdated;
    }


}
?>