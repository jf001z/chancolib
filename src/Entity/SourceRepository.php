<?php
/**
 * Created by PhpStorm.
 * User: jinfeizhang
 * Date: 27/05/2018
 * Time: 14:54
 */

namespace Chancolib\Entity;


use Chancolib\Cache\Cache;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping;

class SourceRepository extends EntityRepository
{
    private $source_key;
    public function __construct($em, Mapping\ClassMetadata $class)
    {
        parent::__construct($em, $class);
        $this->source_key = 'source_key';
    }

    public function getAllSourcesCodesWithCache(){
        $resources = null;
        $cache = new Cache();
        $key = $this->source_key;
        if($cache->has($key)){
            $t = $cache->get($key);
            $resources = json_decode($t,true);
        }else{

            $resources = $this->getAllSourcesCodes();
            $cache->set($key,json_encode($resources));
        }

        return $resources;
    }

    public function getAllSourcesCodes(){
        $resources = null;
        $resources = $this->getEntityManager()
            ->createQuery('SELECT m.sourceId, m.sourceCode,m.sourceName FROM ChancoEntity:Source m')->getResult();
        return $resources;
    }

    public function getSourceBySourceCode($sourceCode){

        $resources = null;
        $resources = $this->getEntityManager()
            ->createQuery('SELECT m.sourceId, m.sourceCode FROM ChancoEntity:Source m WHERE m.sourceCode=:sourceCode')
            ->setParameters(array('sourceCode'=>$sourceCode))
            ->getResult();
        return $resources;

    }
}