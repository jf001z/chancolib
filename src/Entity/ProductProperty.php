<?php

namespace Chancolib\Entity;

use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 *
 */
class ProductProperty
{
    /**
     * @var integer
     */
    private $productPropertyId;
    /**
     *
     */
    private $productPropertyType;

    /**
     *
     */
    private $productPropertyName;

    /**
     *
     */
    private $lastUpdated;

    /**
     * @return int
     */
    public function getProductPropertyId(): int
    {
        return $this->productPropertyId;
    }


    /**
     * @return mixed
     */
    public function getProductPropertyType()
    {
        return $this->productPropertyType;
    }

    /**
     * @param mixed $productPropertyType
     */
    public function setProductPropertyType($productPropertyType): void
    {
        $this->productPropertyType = $productPropertyType;
    }

    /**
     * @return mixed
     */
    public function getProductPropertyName()
    {
        return $this->productPropertyName;
    }

    /**
     * @param mixed $productPropertyName
     */
    public function setProductPropertyName($productPropertyName): void
    {
        $this->productPropertyName = $productPropertyName;
    }

    /**
     * @return mixed
     */
    public function getLastUpdated()
    {
        return $this->lastUpdated;
    }

    /**
     * @param mixed $lastUpdated
     */
    public function setLastUpdated($lastUpdated): void
    {
        $this->lastUpdated = $lastUpdated;
    }

    /**
     * @return mixed
     */
    private $productPropertyAttributes;


    public function __construct()
    {
        $this->productPropertyAttributes = new ArrayCollection();
    }


    /**
     * @return mixed
     */
    public function getProductPropertyAttributes()
    {
        return $this->productPropertyAttributes;
    }




}
?>