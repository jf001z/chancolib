<?php
/**
 * Created by PhpStorm.
 * User: jinfeizhang
 * Date: 28/05/2018
 * Time: 15:06
 */

namespace Chancolib\Entity;


use Chancolib\Cache\Cache;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping;

class GlobalSettingRepository extends EntityRepository
{
    private $setting_key;
    public function __construct($em, Mapping\ClassMetadata $class)
    {
        parent::__construct($em, $class);
        $this->setting_key = 'global_setting';
    }

    public function getSetting(){
        $settings = null;
        $cache = new Cache();
        if($cache->has($this->setting_key)){
            $settings = json_decode($cache->get($this->setting_key),true);
        }else{
            $em = $this->getEntityManager();
            $temp = $em->createQuery('SELECT g FROM ChancoEntity:GlobalSetting g')->getResult();
            $settings = array();
            foreach ($temp as $t){
                $settings[$t->getPropertyName()] = $t->getPropertyValue();
            }
            $cache->set($this->setting_key,json_encode($settings));
        }
        return $settings;
    }

}