<?php
/**
 * Created by PhpStorm.
 * User: jeffzhang
 * Date: 14/04/2018
 * Time: 08:10
 */

namespace Chancolib\Entity;


use Doctrine\ORM;
use Chancolib\Cache\Cache;

/**
 */
class ProductRepository extends ORM\EntityRepository
{
    public function getProductById($id){
        $productBLL = array();
        $product_cache_key = "Product:".$id;
        $cache = new Cache();
        if($cache->has($product_cache_key)) {
            $productBLL = json_decode($cache->get($product_cache_key), true);
            return $productBLL;
        }

        $productDLL = $this->find($id);


        if(!empty($productDLL)){
            $productBLL['productId'] = $productDLL->getProductId();
            $productBLL['productSku'] = $productDLL->getProductSku();
            $productBLL['parentProductId'] = $productDLL->getParentProductId();
            $productBLL['lastUpdated'] = $productDLL->getLastUpdated();
            $productBLL['property'] = array();
            if(!empty($productDLL->getProductPropertyAttributes())){
                foreach ($productDLL->getProductPropertyAttributes() as $property){
                    $productBLL['property'][$property->getProductProperty()->getProductPropertyName()]
                        = [
                            'id' => $property->getProductPropertyId(),
                            'value'=>$property->getProductPropertyAttributeValue()];
                }
            }
            $cache->set($product_cache_key,json_encode($productBLL));
        }else{
            $productBLL = null;
        }
        return $productBLL;
    }
}
