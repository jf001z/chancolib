<?php
/**
 * Created by PhpStorm.
 * User: jinfeizhang
 * Date: 28/05/2018
 * Time: 15:05
 */

namespace Chancolib\Entity;


class GlobalSetting
{
    private $settingId;
    private $propertyName;
    private $propertyValue;
    private $updateTime;

    /**
     * @return mixed
     */
    public function getSettingId()
    {
        return $this->settingId;
    }

    /**
     * @return mixed
     */
    public function getPropertyName()
    {
        return $this->propertyName;
    }

    /**
     * @param mixed $propertyName
     */
    public function setPropertyName($propertyName): void
    {
        $this->propertyName = $propertyName;
    }

    /**
     * @return mixed
     */
    public function getPropertyValue()
    {
        return $this->propertyValue;
    }

    /**
     * @param mixed $propertyValue
     */
    public function setPropertyValue($propertyValue): void
    {
        $this->propertyValue = $propertyValue;
    }

    /**
     * @return mixed
     */
    public function getUpdateTime()
    {
        return $this->updateTime;
    }

    /**
     * @param mixed $updateTime
     */
    public function setUpdateTime($updateTime): void
    {
        $this->updateTime = $updateTime;
    }

}