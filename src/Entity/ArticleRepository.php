<?php
/**
 * Created by PhpStorm.
 * User: jinfeizhang
 * Date: 27/05/2018
 * Time: 11:25
 */

namespace Chancolib\Entity;


use Doctrine\ORM\EntityRepository;
use Symfony\Component\Validator\Constraints\DateTime;

class ArticleRepository extends EntityRepository
{
    public function insertArticle(){
    }
    public function isOldArticle(Article $article){
        $a = $this->findBy(array('sourceId'=>$article->getSourceId(),'publishAt'=>$article->getPublishAt()));
        return !empty($a);
    }

    public function dateFormatTransform($date){

        return preg_replace('/^(\d{4}-\d{2}-\d{2})T(\d{2}:\d{2}:\d{2})[0-9a-zA-Z+.-:]*$/', "$1 $2",$date);
    }
    public function getAllUnprocessedArticles(){

        return $this->findBy(array('processed'=>0));
    }
    public function get10UnprocessedImageArticles(){
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('ar')
            ->from('ChancoEntity:Article','ar')
            ->where("ar.imageUrl <> '' ")
            ->andWhere("ar.imageS3Url = '' ")
            ->orderBy('ar.publishAt','DESC')
            ->setMaxResults(200);

        $query = $qb->getQuery();
        $articles = $query->getResult();
        return $articles;
    }
    public function getLatestArticlesById($sourcCode){
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('ar')
            ->from('ChancoEntity:Article','ar')
            ->where('ar.sourceId=:sourceId')
            ->andWhere('ar.processed=:processed')
            ->orderBy('ar.publishAt','DESC')
            ->setMaxResults(10);
        $qb->setParameters(array(
            'sourceId'=>$sourcCode,
            'processed'=>1
        ));
        $query = $qb->getQuery();
        $articles = $query->getResult();
        $articleArray = array();
        foreach ($articles as $article){

            $tempArticle = array();

            $content = $this->getEntityManager()
                ->getRepository('ChancoEntity:Content')
                ->getContentByArticleId($article->getArticleId());

            $conArray['content'] = $content->getContent();
            $conArray['contentId'] = $content->getContentId();
            $conArray['articleId'] = $content->getArticleId();

            $tempArticle['content'] = $conArray;
            $tempArticle['articleId'] = $article->getArticleId();
            $tempArticle['title'] = $article->getTitle();
            $tempArticle['author'] = $article->getAuthor();
            $tempArticle['sourceId'] = $article->getSourceId();
            $tempArticle['description'] = $article->getDescription();
            $tempArticle['url'] = $article->getUrl();
            $tempArticle['imageUrl'] = $article->getImageUrl();
            $publishTime = $article->getPublishAt();
            $tempArticle['publishAt'] = $publishTime->format('Y-m-d H:i:s');
            $tempArticle['processed'] = $article->getProcessed();
            $tempArticle['processed'] = $article->getProcessed();
            $tempArticle['thumbnailUrl'] = $article->getThumbnailUrl();
            $tempArticle['imageS3Url'] = $article->getImageS3Url();
            $articleArray[] = $tempArticle;
        }
        return $articleArray;
    }
}