<?php
/**
 * Created by PhpStorm.
 * User: jinfeizhang
 * Date: 26/06/2018
 * Time: 23:32
 */

namespace Chancolib\Queue;

use Chancolib\Config\Configuration;
use Pheanstalk\Pheanstalk;

final class BeanstalkClient
{
    private static $client;
    private static $setting;
    protected function __construct(){}
    public static function getInstance(){
        self::$setting = self::getSetting();
        if (key_exists('beanstalk_link',self::$setting) && !empty(self::$setting['beanstalk_link'])){
            $info = explode(':', self::$setting['beanstalk_link']);
            $port = $info[2];
            $host = str_replace('//','',$info[1]);
        }else{
            $port = self::$setting['beanstalk_port'];
            $host = self::$setting['beanstalk_host'];
        }
        self::$client = new Pheanstalk($host,$port);
        if(self::selfTest()){
            return self::$client;
        }else{
            return null;
        }

    }
    protected static function getSetting(){
        if(Configuration::has('beanstalk_info')){
            return Configuration::get('beanstalk_info');
        }else{

            return null;
        }
    }
    public static function selfTest(){
        return self::$client->getConnection()->isServiceListening();
    }

}