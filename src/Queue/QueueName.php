<?php
/**
 * Created by PhpStorm.
 * User: jinfeizhang
 * Date: 27/06/2018
 * Time: 22:36
 */

namespace Chancolib\Queue;


class QueueName
{
    const ARTICLE_GET_CONTENT = 'Article-GetContent';
    const ARTICLE_GET_THUMBNAIL = 'Article-GetThumbnail';
    const ARTICLE_GET_KEYWORDS = 'Article-GetKeywords';
}