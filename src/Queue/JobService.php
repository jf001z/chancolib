<?php
/**
 * Created by PhpStorm.
 * User: jinfeizhang
 * Date: 27/06/2018
 * Time: 22:57
 */

namespace Chancolib\Queue;


class JobService
{
    protected $client;

    public function __construct()
    {
        $this->client = BeanstalkClient::getInstance();
    }

    public function createJob($data,$tube){
        return $this->client->useTube($tube)->put($data);
    }
    public function deleteJob($job){
        return $this->client->delete($job);
    }
    public function getJob($tube){
        return $this->client->watch($tube)->ignore('default')->reserve();
    }

    public function bureyJob($job){
        return $this->client->bury($job);
    }
}